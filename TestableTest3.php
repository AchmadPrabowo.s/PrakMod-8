<?php 
namespace PHPUnit\Framework;
use PHPUnit\Framework\TestCase;
include "Testable.php";

class TestableTest3 extends TestCase{ 
	private $_testable = null;
	public function setUp(){
		$this->_testable = new Testable();
	}
	public function tearDown(){
		$this->_testable = null;
	}
	public function testStringEnding(){
		$testString = $this->_testable->getTestString();
		$this->assertStringEndsWith('string',$testString);
	}
	public function testStringStarts(){
		$testString = $this->_testable->getTestString();
		$this->assertStringStartsWith('I',$testString);
	}
	public function testEqualFileContents(){
		$this->assertStringEqualsFile('textfile.txt','foo');
	}
	public function testDoesStringMatchFormat(){
		$testString = $this->_testable->getTestString();
		$this->assertStringMatchesFormat('%s',$testString);
	}
	public function testDoesStringFileFormat(){
		$testString = $this->_testable->getTestString();
		$this->assertStringMatchesFormatFile('textfile.txt','foo');
		}
	}
?>