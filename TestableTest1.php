<?php
	namespace PHPUnit\Framework;
	use PHPUnit\Framework\TestCase;
	include "Testable.php";
	class TestableTest1 extends TestCase{
		private $_testtable = null;
		public function setUp(){
			$this->_testable = new Testable();
		}
		public function tearDown(){
			$this->_testable = null;
		}
		//true or false
		public function testTruePropertyIsTrue(){
			$this->assertTrue($this->_testable->trueProperty,"trueProperty isn't true");
		}
		public function testTruePropertyIsFalse(){
			$this->assertFalse($this->_testable->falseProperty,"trueProperty isn't false");
		}
	}
?>