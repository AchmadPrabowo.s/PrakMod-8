<?php
	namespace PHPUnit\Framework;
	use PHPUnit\Framework\TestCase;
	include "Testable.php";
	class TestableTest extends TestCase{
		private $_testable = null;
		public function setUp(){
			$this->_testable = new Testable();
		}
		public function tearDown(){
			$this->_testable = null;
		}
	}
?>