<?php 
namespace PHPUnit\Framework;
use PHPUnit\Framework\TestCase;
include "Testable.php";
class TestableTes1 extends TestCase{ 
	private $_testable = null;
	public function setUp(){
		$this->_testable = new Testable();
	}
	public function tearDown(){
		$this->_testable = null;
	}
	//mathmagic
	public function testValueEquals(){
		$valueOne = 4;
		$valueTwo = 2;
		$this->assertEquals($this->_testable->addValues($valueOne,$valueTwo),6);
	}
	public function testValueGreaterThan(){
		$valueOne = 4;
		$valueTwo = 2;
		$this->assertGreaterThan($valueTwo,$valueOne);
	}
	public function testLessThanOrEqual(){
		$valueOne = 2;
		$valueTwo = 4;
		$this->assertLessThanOrEqual($valueTwo,$valueOne);
	}
	public function testAreObjectsEqual(){
		$testTwo = new Testable();
		$this->_testable->resetMe = true;
		$this->assertEquals($this->_testable,$testTwo);
	}
}
?>
		