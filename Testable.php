<?php
	namespace PHPUnit\Framework;
	class Testable{
		public $trueProperty = true;
		public $falseProperty = false;
		
		public $resetMe = true;
		public $testArray = array(
			'first key' => 1,
			'second key' => 2
		);
		private $testString = "I do love some string";
		public function _construct(){
		}
		public function addValues ($valueOne, $valueTwo){
			return $valueOne+$valueTwo;
		}
		public function getTestString(){
			return $this->testString;
		}
	}
?>
	